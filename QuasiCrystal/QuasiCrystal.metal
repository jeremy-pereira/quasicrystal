//
//  QuasiCrystal.metal
//  QuasiCrystal
//
//  Created by Jeremy Pereira on 27/04/2019.
//
// Copyright 2019 Jeremy Pereira
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <metal_stdlib>
#include "QuasiCrystal.h"

using namespace metal;

float3 average(float3 in, int count)
{
	return in / count;
}

float wrap(float in)
{
	float integral;
	float fraction = modf(in, integral);
	if ((int)integral % 2 == 1)
	{
		return 1 - fraction;
	}
	else
	{
		return fraction;
	}
}

float3 wrap(float3 in)
{
	return float3(wrap(in.x), wrap(in.y), wrap(in.z));
}

float computeAnAmplitude(float2 point, float angle, float k, float wt)
{
	float x = point.x * cos(angle) + point.y * sin(angle);
	return (cos(k * x - wt) + 1) / 2;
}

kernel void quasiCrystalRender(texture2d<float, access::write> output [[texture(0)]],
							   constant Environment *environment [[buffer(0)]],
							   constant WaveParameters *waveParams [[buffer(1)]],
						       uint2 gid [[thread_position_in_grid]])
{
	float h = (float)output.get_height();
	float w = (float)output.get_width();
	// y subtracted from 1 to get maths coordinate convention
	float2 absPoint = float2((float)gid.x / h, 1 - (float)gid.y / h);
	float2 origin = float2(w / h, 1) / 2;

	float2 point = absPoint - origin;
	int waveCount = environment->waveCount;

	float3 yTotal = float3(0, 0, 0);
	for (int i = 0 ; i < waveCount ; ++i)
	{
		float k = 2 * M_PI_F / waveParams[i].waveLength;
		float wt = 2 * M_PI_F * waveParams[i].frequency * environment->time;
		float y_r = computeAnAmplitude(point, waveParams[i].angle, k, wt);
		if (environment->colour == 0)
		{
			yTotal += float3(y_r, y_r, y_r);
		}
		else
		{
			float y_g = computeAnAmplitude(point, waveParams[i].angle + 2 * M_PI_F / 3, k, wt);
			float y_b = computeAnAmplitude(point, waveParams[i].angle + 4 * M_PI_F / 3, k, wt);
			yTotal += float3(y_r, y_g, y_b);
		}
	}
	switch (environment->combiner)
	{
		case wrapFunction:
			yTotal = wrap(yTotal);
			break;
		case averageFunction:
			yTotal = average(yTotal, waveCount);
			break;
		default:
			yTotal = wrap(yTotal);
			break;
	}

	float4 colour = float4(yTotal, 1);

	output.write(colour, gid);
}

