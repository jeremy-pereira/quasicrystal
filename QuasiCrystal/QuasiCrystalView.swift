//
//  QuasiCrystalView.swift
//  QuasiCrystal
//
//  Created by Jeremy Pereira on 27/04/2019.
//
// Copyright 2019 Jeremy Pereira
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Cocoa
import MetalKit

protocol QuasiCrystalViewSource: class
{
	var waves: [WaveParameters] { get }
	var isColoured: Bool { get }
	var combiner: Combiner { get }
}

class QuasiCrystalView: MTKView
{
	private let framesPerSecond = 60
	private let commandQueue: MTLCommandQueue
	private let computePipelineState: MTLComputePipelineState
	private var waveBuffer: MTLBuffer?
	private var environmentBuffer: MTLBuffer?

	weak var waveSource: QuasiCrystalViewSource?

	required init(coder: NSCoder)
	{
		guard let device = MTLCreateSystemDefaultDevice()
			else { fatalError("Cannot obtain the GPU device") }
		do
		{
			guard let commandQueue = device.makeCommandQueue()
				else { throw QuasiCrystalView.Error.unexpectedNil("command queue") }
			self.commandQueue = commandQueue
			computePipelineState = try QuasiCrystalView.registerShaders(device: device)
		}
		catch
		{
			fatalError("\(error)")
		}
		super.init(coder: coder)
		self.device = device
		framebufferOnly = false
		preferredFramesPerSecond = framesPerSecond
	}

	override init(frame frameRect: CGRect, device: MTLDevice?)
	{
		let notNilDevice = device ?? MTLCreateSystemDefaultDevice()!
		do
		{
			guard let commandQueue = notNilDevice.makeCommandQueue()
				else { throw QuasiCrystalView.Error.unexpectedNil("command queue") }
			self.commandQueue = commandQueue
			computePipelineState = try QuasiCrystalView.registerShaders(device: notNilDevice)
		}
		catch
		{
			fatalError("\(error)")
		}
		super.init(frame: frameRect, device: notNilDevice)
		framebufferOnly = false
		preferredFramesPerSecond = framesPerSecond
	}

	private var iterations = 0
	private var environment = Environment(time: 0, waveCount: 0, colour: 0, combiner: wrapFunction)

	override func draw(_ dirtyRect: NSRect)
	{
		super.draw(dirtyRect)

		guard let drawable = currentDrawable, let device = device
			else { return }

		// Get the waves from the wave source and make sure there is at least
		// one of them.
		guard let waves = self.waveSource?.waves else { return }
		guard waves.count > 0 else { return }
		let isColoured = waveSource?.isColoured ?? false
		let combiner = waveSource?.combiner ?? wrapFunction
		do
		{
			guard let commandBuffer = commandQueue.makeCommandBuffer()
				else { throw QuasiCrystalView.Error.unexpectedNil("command buffer") }
			guard let commandEncoder = commandBuffer.makeComputeCommandEncoder()
				else { throw QuasiCrystalView.Error.unexpectedNil("command encoder") }
			commandEncoder.setComputePipelineState(computePipelineState)
			commandEncoder.setTexture(drawable.texture, index: 0)

			try makeSureBuffersAreAllocated(device: device, newWaveCount: waves.count)

			environment.time = Float(iterations) / Float(framesPerSecond)
			environment.waveCount = Int32(waves.count)
			environment.colour = isColoured ? 1 : 0
			environment.combiner = combiner

			environmentBuffer?.contents().copyMemory(from: &environment, byteCount: MemoryLayout<Environment>.stride)
			waveBuffer?.contents().copyMemory(from: waves, byteCount: MemoryLayout<WaveParameters>.stride * waves.count)
			commandEncoder.setBuffer(environmentBuffer, offset: 0, index: 0)
			commandEncoder.setBuffer(waveBuffer, offset: 0, index: 1)

			let w = computePipelineState.threadExecutionWidth
			let h = computePipelineState.maxTotalThreadsPerThreadgroup / w
			let threadsPerGroup = MTLSizeMake(w, h, 1)
			commandEncoder.dispatchThreads(MTLSizeMake(drawable.texture.width,
													   drawable.texture.height,
													   1),
										   threadsPerThreadgroup: threadsPerGroup)
			commandEncoder.endEncoding()
			commandBuffer.present(currentDrawable!)
			commandBuffer.commit()

			iterations = iterations &+ 1
		}
		catch
		{
			print("\(error)")
		}
	}

	static func registerShaders(device: MTLDevice) throws -> MTLComputePipelineState
	{
		let library = device.makeDefaultLibrary()!
		guard let kernel = library.makeFunction(name: "quasiCrystalRender")
			else { throw QuasiCrystalView.Error.unexpectedNil("kernel does not exist") }

		return try device.makeComputePipelineState(function: kernel)
	}

	private func makeSureBuffersAreAllocated(device: MTLDevice, newWaveCount: Int) throws
	{
		if waveBuffer == nil || newWaveCount != Int(environment.waveCount)
		{
			guard let newBuffer = device.makeBuffer(length: MemoryLayout<WaveParameters>.stride * newWaveCount,
													options: [])
				else { throw QuasiCrystalView.Error.unexpectedNil("allocating wave buffer") }
			waveBuffer = newBuffer
		}
		if environmentBuffer == nil
		{
			guard let newBuffer = device.makeBuffer(length: MemoryLayout<Environment>.stride,
													options: [])
				else { throw QuasiCrystalView.Error.unexpectedNil("allocating environment buffer") }
			environmentBuffer = newBuffer
		}
	}
}

extension QuasiCrystalView
{
	enum Error: Swift.Error
	{
		case unexpectedNil(String)
	}
}
