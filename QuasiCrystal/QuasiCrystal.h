//
//  QuasiCrystal.h
//  QuasiCrystal
//
//  Created by Jeremy Pereira on 28/04/2019.
//
// Copyright 2019 Jeremy Pereira
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef QuasiCrystal_h
#define QuasiCrystal_h

enum Combiner
{
	wrapFunction = 0,
	averageFunction = 1
};

/**
 The parameters for an individual wave.
 */
struct WaveParameters
{
	/// Wavewlength as a proportion of the height of the view.
	float waveLength;
	/// Frequency of the wave.
	float frequency;
	/// Direction in which wave travels using standard maths conventions.
	float angle;
};

/**
 The general environment in which the waves are generated.
 */
struct Environment
{
	/// The current time in seconds.
	float time;
	/// The number of waves
	int32_t waveCount;
	/// Whether to use colour or not
	int32_t colour;
	/// Whuich combiner to use to combine the values for each wave
	enum Combiner combiner;
};

#endif /* QuasiCrystal_h */
