//
//  QuasiCrystalController.swift
//  QuasiCrystal
//
//  Created by Jeremy Pereira on 27/04/2019.
//
// Copyright 2019 Jeremy Pereira
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Cocoa

class QuasiCrystalController: NSViewController, QuasiCrystalViewSource
{

	private var _waves: [WaveParameters] = []
	{
		didSet
		{
			waveTable.reloadData()
		}
	}


	/// The list of waves to use in generating the crystal.
	///
	/// The list is recreated any time the number of waves is changed by the
	/// user.
	internal var waves: [WaveParameters]
	{
		let expectedCount = numberOfWavesField.integerValue
		if _waves.count != expectedCount
		{
			let angle = Float.pi / Float(expectedCount)
			_waves = (0 ..< expectedCount).map
			{
				let thisAngle = Float($0) * angle
				return WaveParameters(waveLength: 0.02, frequency: 0.5, angle: thisAngle)
			}
		}
		return _waves
	}

	@IBOutlet var quasiCrystalView: QuasiCrystalView!
	@IBOutlet var numberOfWavesField: NSTextField!
	@IBOutlet var numberOfWavesFormatter: NumberFormatter!
	@IBOutlet var colourCheckBox: NSButton!
	@IBOutlet var combinerPopup: NSPopUpButton!
	@IBOutlet var waveTable: NSTableView!

	let defaultWaveCount = 7

    override func viewDidLoad()
	{
        super.viewDidLoad()
		numberOfWavesFormatter.maximumFractionDigits = 0
		angleFormatter.maximumFractionDigits = 2
		numberOfWavesField.integerValue = defaultWaveCount
		quasiCrystalView.waveSource = self
		waveTable.reloadData()
		angleFormatter.numberStyle = .decimal
		angleFormatter.maximumFractionDigits = 2
    }

	var isColoured: Bool { return colourCheckBox.state == NSControl.StateValue.on }

	var combiner: Combiner
	{
		guard let selectedItem = combinerPopup.selectedItem
			else { return wrapFunction }

		return Combiner(UInt32(selectedItem.tag))
	}

	let angleFormatter = NumberFormatter()
}

extension QuasiCrystalController: NSTableViewDataSource
{
	func numberOfRows(in tableView: NSTableView) -> Int
	{
		return waves.count
	}

	func tableView(_ tableView: NSTableView, objectValueFor tableColumn: NSTableColumn?, row: Int) -> Any?
	{
		guard let columnIdentifier = tableColumn?.identifier else { return "" }

		if columnIdentifier.rawValue == "waveLength"
		{
			return waves[row].waveLength
		}
		else if columnIdentifier.rawValue == "frequency"
		{
			return waves[row].frequency
		}
		else if columnIdentifier.rawValue == "angle"
		{
			return waves[row].angleInDegrees
		}
		else
		{
			fatalError("Invalid column identifier \(columnIdentifier)")
		}
	}

	func tableView(_ tableView: NSTableView, setObjectValue object: Any?, for tableColumn: NSTableColumn?, row: Int)
	{
		guard let column = tableColumn else { return }
		if column.identifier.rawValue == "waveLength"
		{
			guard let wavelength = object as? Float
			else
			{
				print("Object not a float: \(object.debugDescription)")
				return
			}
			_waves[row].waveLength = wavelength
		}
	}

	@IBAction func waveLengthEdited(sender: Any)
	{
		guard let waveLengthField = sender as? NSTextField else { return }

		let row = waveTable.row(for: waveLengthField)
		_waves[row].waveLength = waveLengthField.floatValue
	}


	@IBAction func frequencyEdited(_ sender: Any)
	{
		guard let frequencyField = sender as? NSTextField else { return }

		let row = waveTable.row(for: frequencyField)
		_waves[row].frequency = frequencyField.floatValue
	}

	@IBAction func angleEdited(_ sender: Any)
	{
		guard let angleField = sender as? NSTextField else { return }

		let row = waveTable.row(for: angleField)
		_waves[row].angleInDegrees = angleField.floatValue
	}

}

extension QuasiCrystalController: NSTableViewDelegate
{
	func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView?
	{
		guard let column = tableColumn else { fatalError("No column supplied") }
		let cell = tableView.makeView(withIdentifier: column.identifier, owner: self) as! NSTableCellView
		cell.textField!.objectValue = self.tableView(tableView, objectValueFor: column, row: row)
		cell.textField!.formatter = angleFormatter
		return cell
	}
}

extension WaveParameters
{
	var angleInDegrees: Float
	{
		get { return self.angle * 180 / Float.pi }
		set { self.angle = newValue * Float.pi / 180 }
	}
}
